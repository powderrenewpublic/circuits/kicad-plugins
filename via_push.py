import pcbnew
import os

class PushViaBottomDownPlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Move Via Bottom Layer Down"
        self.category = "Via Tools"
        self.description = "Move the bottom layer of the selected via(s) " \
            "down a layer"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__),
                                           'go-down.png')

    def Run(self):
        board = pcbnew.GetBoard()
        selected_vias = [_ for _ in board.GetTracks()
                         if _.IsSelected() and _.Type() == pcbnew.PCB_VIA_T]

        layer_set = board.GetEnabledLayers()
        copper_stack = list(layer_set.CuStack())

        for via in selected_vias:
            bottom_layer = via.BottomLayer()
            bottom_idx = copper_stack.index(bottom_layer)
            if bottom_idx == len(copper_stack) - 1:
                continue
            via.SetBottomLayer(copper_stack[bottom_idx+1])


class PushViaTopUpPlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Move Via Top Layer Up"
        self.category = "Via Tools"
        self.description = "Move the top layer of the selected via(s) " \
            "up a layer"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__),
                                           'go-up.png')

    def Run(self):
        board = pcbnew.GetBoard()
        selected_vias = [_ for _ in board.GetTracks()
                         if _.IsSelected() and _.Type() == pcbnew.PCB_VIA_T]

        layer_set = board.GetEnabledLayers()
        copper_stack = list(layer_set.CuStack())

        for via in selected_vias:
            bottom_layer = via.TopLayer()
            bottom_idx = copper_stack.index(bottom_layer)
            if bottom_idx == 0:
                continue
            via.SetTopLayer(copper_stack[bottom_idx-1])


PushViaBottomDownPlugin().register()
PushViaTopUpPlugin().register()
