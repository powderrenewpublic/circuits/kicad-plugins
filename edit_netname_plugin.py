import pcbnew
import os
import wx

class WxClipboardContext:
    def __init__(self):
        self.opened = False

    def __enter__(self):
        if wx.TheClipboard.Open():
            self.opened = True
            return wx.TheClipboard
        else:
            return None

    def __exit__(self, a, b, c):
        if self.opened:
            wx.TheClipboard.Close()


class PasteNetNamePlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Paste Net Name"
        self.category = "Copy/Paste"
        self.description = "Paste the net name to a pad"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__),
                                           'edit-paste.png')

    def Run(self):
        result = wx.TextDataObject()
        with WxClipboardContext() as wx_clipboard:
            if wx_clipboard is not None:
                wx_clipboard.GetData(result)
                net_name = result.GetText()
            else:
                print("Can't open")
                return

        board = pcbnew.GetBoard()
        net = board.FindNet(net_name)
        net_code = net.GetNetCode()
        if net is not None:
            selected_pads = [_ for _ in board.GetPads() if _.IsSelected()]
            for pad in selected_pads:
                pad.SetNetCode(net_code)


class CopyNetNamePlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Copy Net Name"
        self.category = "Copy/Paste"
        self.description = "Copy the net name from a pad"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__),
                                           'edit-copy.png')

    def Run(self):
        board = pcbnew.GetBoard()
        selected_pads = [_ for _ in board.GetPads() if _.IsSelected()]

        if len(selected_pads) != 1:
            # Use a dialog
            dlg = wx.MessageDialog(None, "Oops, not exactly one pad selected!")
            dlg.ShowModal()
            print("Oops, not exactly one thing selected")
            return

        text = wx.TextDataObject(selected_pads[0].GetNetname())
        with WxClipboardContext() as wx_clipboard:
            if wx_clipboard is not None:
                wx_clipboard.SetData(text)
            else:
                print("Can't open")


class SetTrackNetPlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Set Track Net"
        self.category = "Backannotate"
        self.description = "Set net name on track net from pad net"
        self.show_toolbar_button = True
        self.icon_file_name = os.path.join(os.path.dirname(__file__),
                                           'tag-new.png')

    def Run(self):
        board = pcbnew.GetBoard()
        selected_pads = [_ for _ in board.GetPads() if _.IsSelected()]

        if len(selected_pads) != 1:
            # Use a dialog
            dlg = wx.MessageDialog(None, "Oops, not exactly one pad selected!")
            dlg.ShowModal()
            print("Oops, not exactly one pad selected")
            return

        net = selected_pads[0].GetNet()

        # Traces and vias both have the same parent class track
        selected_wiring = [_ for _ in board.GetTracks() if _.IsSelected()]

        if len(selected_wiring) != 1:
            # Use a dialog
            dlg = wx.MessageDialog(None,
                                   "Oops, not exactly one track/via "
                                   "selected!")
            dlg.ShowModal()
            print("Oops, not exactly one track/via selected")
            return

        wire = selected_wiring[0]

        connectivity = board.GetConnectivity()
        types = [pcbnew.PCB_TRACE_T, pcbnew.PCB_PAD_T, pcbnew.PCB_VIA_T,
                 pcbnew.PCB_FOOTPRINT_T]
        connected_items = connectivity.GetConnectedItems(wire, types, True)

        for item in connected_items:
            if item.Type() in [pcbnew.PCB_PAD_T]:
                item.SetNetCode(net.GetNetCode())


CopyNetNamePlugin().register()
PasteNetNamePlugin().register()
SetTrackNetPlugin().register()
